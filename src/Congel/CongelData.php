<?php


namespace App\Congel;

use App\Entity\Congel;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;


class CongelData
{
    /**
     * @Assert\NotBlank(message="Indiquez le nom de vrotre congélateur")
     * @ORM\Column(type="string")
     */
    public string $nom;

    /**
     * @Assert\NotBlank(message="Votre congélateur doit avoir au moins un tiroir sinon...")
     * @Assert\GreaterThan(0)
     */
    public int $nbTiroirs;
    public Uuid $identificateur;


    public function fromCongelEntity(Congel $congel): CongelData
    {
        $this->nom = $congel->getNom();
        $this->nbTiroirs = $congel->getNbTiroirs();
        $this->identificateur=$congel->getIdentificateur();
        return $this;
    }
}