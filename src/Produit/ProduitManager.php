<?php


namespace App\Produit;


use App\Entity\Produit;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class ProduitManager
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function CreateProduit(ProduitData $produitData)
    {
        $produit = new Produit();
        $produit->setNomproduit($produitData->Nomproduit);
        $produit->setCategory($produitData->category);
        $this->entityManager->persist($produit);
        $this->entityManager->flush();
    }

    public function UpdateProduit(ProduitData $produitData, Produit $produit)
    {
        if ($produitData->Nomproduit == "") {
            throw new Exception('Le nom du produit est obligatoire');
        }
        $produit->setNomproduit($produitData->Nomproduit);
        $produit->setCategory($produitData->category);
        $this->entityManager->persist($produit);
        $this->entityManager->flush();
    }

    public function DeleteProduit(Produit $produit)
    {
        $this->entityManager->remove($produit);
        $this->entityManager->flush();
    }
}