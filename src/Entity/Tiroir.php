<?php

namespace App\Entity;

use App\Repository\TiroirRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TiroirRepository::class)
 */
class Tiroir
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity=Congel::class, inversedBy="tiroirs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $congel;

    public function __toString():string
    {
       return $this->getCongel()->getNom()." : ".$this->getNom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getCongel(): ?Congel
    {
        return $this->congel;
    }

    public function setCongel(?Congel $congel): self
    {
        $this->congel = $congel;

        return $this;
    }
}
