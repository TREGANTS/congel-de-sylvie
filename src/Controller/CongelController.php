<?php

namespace App\Controller;

use App\Congel\CongelData;
use App\Congel\CongelManager;
use App\Entity\Category;
use App\Entity\Congel;
use App\Form\CongelType;
use App\Form\CongelUpdType;
use App\Repository\CongelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CongelController extends AbstractController
{
    /**
     * @Route("/congel",name="congel")
     * @param CongelRepository $repository
     * @return Response
     */
    public function index(CongelRepository $repository): Response
    {
        $congelateurs = $repository->findAll();
        return $this->render('congel/index.html.twig', [
            'congelateurs' => $congelateurs,
        ]);
    }

    /**
     * @Route("/congel/add",name="addcongel",methods={"GET","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function Add(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CongelType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $CongelManager = new CongelManager($em);
            $CongelManager->CreateCongel($form->getData());
            $this->addFlash('success', 'Nouveau congélateur enregistré');
            return  $this->redirectToRoute('congel');


        }
        return $this->render('congel/new.html.twig', [
            'congel_add' => $form->createView(),
        ]);
    }

    /**
     * @Route("/congel/edit/{id}",name="editcongel",methods={"GET","POST"})
     * @param Congel $congel
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @throws \Exception
     */
    public function edit(Congel $congel,Request $request, EntityManagerInterface $em): Response
    {

        $congelDisplay = new CongelData();
        $congelDisplay->fromCongelEntity($congel);
        $form = $this->createForm(CongelUpdType::class,$congelDisplay);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $CongelManager = new CongelManager($em);
            $CongelManager->UpdateCongel($form->getData(), $congel);
            $this->addFlash('success', 'Modification enregistrée');
           return  $this->redirectToRoute('congel');
        }
        return $this->render('congel/edit.html.twig', [
            'congel_edit' => $form->createView(),
        ]);


    }
    /**
     * @Route("/congel/delete/{id}",name="deletecongel",methods={"DELETE"})
     */
    public function delete(Congel $congel,EntityManagerInterface $entityManager): Response
    {
        $CongelManager = new CongelManager($entityManager);
        $CongelManager->DeleteCongel($congel);
        $this->addFlash('success', 'Suppression effectuée');
        return $this->redirectToRoute('congel');
    }
}

