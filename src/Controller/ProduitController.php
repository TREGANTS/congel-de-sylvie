<?php


namespace App\Controller;



use App\Congel\CongelManager;

use App\Entity\Produit;

use App\Form\ProduitType;
use App\Produit\ProduitData;
use App\Produit\ProduitManager;

use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    /**
     * @Route("/produit",name="produit")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function index(ProduitRepository $repository): Response
    {
        $produits = $repository->findAll();
        return $this->render('produit/index.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("produit/add",name="addproduit",methods={"GET","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $em):Response
    {
        $form = $this->createForm(ProduitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $produitManager = new ProduitManager($em);
            $produitManager->CreateProduit($form->getData());
            $this->addFlash('success', 'Nouveau Produit  enregistré');
            return $this->redirectToRoute('produit');


        }
        return $this->render('produit/new.html.twig', [
            'produit_add' => $form->createView(),
        ]);
    }
    /**
     * @Route("/produit/edit/{id}",name="editproduit",methods={"GET","POST"})
     * @param Produit $produit
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(Produit $produit,Request $request, EntityManagerInterface $em): Response
    {

        $ProduitDisplay = new ProduitData();
        $ProduitDisplay->fromProduitEntity($produit);
        $form = $this->createForm(ProduitType::class,$ProduitDisplay);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $produitManager = new ProduitManager($em);
            $produitManager->UpdateProduit($form->getData(), $produit);
            $this->addFlash('success', 'Modification enregistrée');
            return  $this->redirectToRoute('produit');
        }
        return $this->render('produit/edit.html.twig', [
            'produit_edit' => $form->createView(),
        ]);
    }

    /**
     * @Route("/produit/delete/{id}",name="deleteproduit",methods={"DELETE"})
     * @param Produit $produit
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
        public function delete(Produit $produit,EntityManagerInterface $entityManager): Response
    {
        $produitManager = new ProduitManager($entityManager);
        $produitManager->DeleteProduit($produit);
        $this->addFlash('success', 'Suppression effectuée');
        return $this->redirectToRoute('produit');
    }


}